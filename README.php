<?php

/* 
 *      FOR RYAN's Project notes
 * 
 *   project is running on port 7771 as per tutorial:
 *      http://wern-ancheta.com/blog/2015/05/09/getting-started-with-lumen/
 *  & can be accessed @ 
 *      http://localhost:7771/
 *  after entering $ php artisan serve --port=7771 
 *      in the console inside of /lumin
 * 
 * running as: http://localhost/reactionclip/lumen/public/
 * per: http://stackoverflow.com/questions/29713635/lumen-not-working-out-of-the-box
 *
 * 
 * 
 * 
 * -../home
 *      +this will be the entry page ppl see when they link into reactionclip.com
 *          -all the available clips in order of views LIMIT 10 with offset buttons
 *          -on this page the clips are thumbnails that play on mouseover and have the uploader's name and the description beneath them.
 *              +if a video is clicked the user is sent to clip/[clip_id]
 * 
 * -../download
 *      +registered user may download a video from youtube here.  when it's downloaded they are moved to:
 * 
 * -../editclip
 *      +here the user edits the clip, adds a description
 * 
 * -../user/[name]
 *      +user's page
 *          -shows all the posts that this user has made
 * 
 * -..user/[name]/favorites
 *      +shows only clips that the user has a registered click on a tag EXCEPT the sucks tag
 * 
 * -../clip/[clip_id]
 *      +shows ONLY the clip
 * 
 * -../login
 *      -login/logout
 * 
 * -../register
 *      register user
 * 
 * 
 * 
 * 
 * ideas for v2.0:
 *  -let users see how many times *their* video has been accessed and from what sites
 *  -tagging system users can vote on a set of 6(?) PREMADE tags: new, cool, funny, smart, hot & sucks
 *      +these tags organize the clips. when a clip is new it has 5 votes already in new
 *         + clips are ALWAYS in the column where they've recieved the most tags so it won't leave the new column until it is voted somewhere else but it can always be moved by enough votes in a different direction.
 */

