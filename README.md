# Reactionclip.com #

[Reactionclip.com](http://reactionclip.com) is a website where users can curl/download youtube videos to the server and then edit the time so that they can share a small clip of a video with friends.

* Version 1.0

### Features ###

* Built in lumen 5.1 **will not work in 5.2** don't upgrade. (5.2 was redesigned as a micro-service MVC & doesn't use views)
* requires:
    * cUrL
    * php 5.5+
    * ffmpeg

### Who do I talk to? ###

* RyanMayo