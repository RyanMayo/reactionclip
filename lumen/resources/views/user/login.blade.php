@extends('partials.master')

@section('title')
<div class='medium-8 medium-offset-2 columns'>
    <h2>Login</h2>
</div>
@endsection

@section('content')
<div class='medium-8 medium-offset-2 columns '>
    <form action="/login" method="post">
        User name: <input type="text" name="user_name"><br>
        User Password: <input type="text" name="user_password"><br>
        <input type="submit" id='login_submit'>
    </form>
<label class='button success' for="login_submit">Submit</label>
<a href="/register" class='button' >New Account</a>
</div>
@endsection