
@extends('partials.master')

@section('title')
<div class='medium-8 medium-offset-2 columns '>
<h3>register</h3>
</div>
@endsection

@section('content')
<div class='medium-8 medium-offset-2 columns '>
<form action='/register' method="post">
    User name: <input type="text" name="user_name"><br>
    Password: <input type="text" name="user_password"><br>
    Confirm Password: <input type="text" name="confirm_password"><br>
    <input class='button success' type="submit">
</form>
</div>
@endsection