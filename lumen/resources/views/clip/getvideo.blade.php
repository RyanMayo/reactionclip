@extends('partials.master')

@section('title')

<div class='medium-8 medium-offset-2 columns'>
    <h2>Get a video</h2>
</div>
@endsection

@section('content')
<div  class='medium-8 medium-offset-2 columns'>
<div id='edit_suggestion' class="callout warning">
    <p>Put the part in red into the link field<br>https://www.youtube.com/watch?v=<span id='span_hilite'>lvEzTjuM81A</span></p>
    <p>Please note that this is an active beta and can/will have some bugs. I.E. If you get the error screen just come back to this page and try again before giving up.  Please bear with me.  Also, certain types of copyrighted videos will not download.  I'm working on this too.  Thanks -R.M.</p>
</div>
</div>
<div class='medium-8 medium-offset-2 columns '>
    <form action="<?php echo 'http://'. $_SERVER['SERVER_NAME'].'/clip/getoriginal' ?>" method="post">
        <div class='input-group'>
            <span class="input-group-label">Link</span>
            <input type="text" class="input-group-field" name="file" value="lvEzTjuM81A" placeholder="lvEzTjuM81A">
            <div class="input-group-button">
                <input type="submit" class="button success" value="Submit">
            </div>
        </div>
    </form>
</div>
@endsection