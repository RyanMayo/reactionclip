
@extends('partials.master')

@section('title')
<div class='small-10 small-offset-1 columns'>
    <h2>finalize and save</h2>
</div>

@endsection

@section('content')
<div class='small-10 small-offset-1 columns'>
    @include('partials.clip', ['clip_id' => $input['clip_id'], 'file_loc' => 'clips' ])
</div>
<div class='small-10 small-offset-1 columns'>
    <form action="/posts/insertclip" method="post">
        <input type="hidden" value="<?php echo $input['clip_id'] ?>" name="file">
        
            <label>
                description:
                <textarea placeholder="Please describe the clip in a way that other users can search for it." name='desc'></textarea>
            </label>
        
        <input type="submit" id='submit-form'>
    </form>
    <form action="<?php echo '/clip/edit/'.$input['clip_id'] ?>" method="post">
        
        <input type="hidden" value="TRUE" name="delete_clip">
        <input type="submit" id='redo-form'>
    </form>
    <label class='button success' for="submit-form">Save</label>
    <label class='button alert' for="redo-form">Redo</label>
</div>
@endsection