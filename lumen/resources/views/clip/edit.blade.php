<?php
//from ClipController@editClip
?>
@extends('partials.master')

@section('title')

<div class='small-10 small-offset-1 columns'>
    <h2>edit clip:</h2>
</div>
@endsection

@section('content')
    <div class='small-10 small-offset-1 columns'>
        @include('partials.clip', ['clip_id' => $input['clip_id'], 'file_loc' => 'clip_original' ])
    </div>
    <div  class='small-10 small-offset-1 columns'>
        <div id='edit_suggestion' class="callout warning">
            <p>Inputs must be input as seconds so 1:26 would be 86, you can also use fractions of a second in decimal form: 86.25</p>
            <p>Clips may not be longer than 30 seconds in length (pending) </p>
        </div>

            <form action="<?php echo 'http://'. $_SERVER['SERVER_NAME'].'/clip/make' ?>" method="post">
                <input type="hidden" value="<?php echo $input['clip_id'] ?>" name="clip_id">
                <div class='medium-6 columns'>
                    <div class="input-group">
                        <span class="input-group-label">Start</span>
                        <input type="text" class="input-group-field" value="<?php if(isset($input['start_cut'])){echo $input['start_cut'];} ?>" name="start_cut">
                    </div>
                </div>
                <div class='medium-6 columns'>
                    <div class='input-group'>
                        <span class='input-group-label' >End</span>
                        <input type="text" class="input-group-field" value="<?php if(isset($input['end_cut'])){echo $input['end_cut'];} ?>" name="end_cut" >
                        <div class="input-group-button">
                            <input type="submit" class="button success" value="Submit">
                        </div>
                    </div>
                </div>
            </form>

    </div>
    

@endsection