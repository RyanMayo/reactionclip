@extends('partials.master')

@section('title')
    <div class='small-10 small-offset-1 columns'>
        <br>
    </div>
@endsection

@section('content')

@foreach($posts as $i)

@if(strpos($i->name, '.mp4') || strpos($i->name, '.webm'))

<div class='small-10 small-offset-1 columns dealie'>
    <div class='clip-container'>
        <?php
            if(strpos($i->name, '.mp4') || strpos($i->name, '.webm' ))
            {
                $ext = explode('.', $i->name)[1];
                
                    echo '<video class="video-clip" controls><source src='
                .url('/files/clips/'.$i->name).' type="video/'.$ext.
                            '">Your browser does not support the video tag.</video>';

                    echo '</div>';
                    echo '<p id="sub_vid_text" class="float-right"><a href="/clip/'.$i->name.'">'.$i->name.'</a>  Created on: '. $i->created_at.' by: '. htmlspecialchars($i->user_name).' & viewed '.$i->views.' times.</p>';
                    echo '<p id="video_description">'.htmlspecialchars($i->description).'</p>';
                   
            }else{
                echo 'no usable file at found at: '.base_path(). '/public/files/clips/'.$i->name;
            }
         ?>
        
    </div>    
</div>
    
@endif
@endforeach
@endsection