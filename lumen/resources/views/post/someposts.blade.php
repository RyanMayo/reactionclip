
@extends('partials.master')

@section('title')
<div class='row'>
    <div class='small-12 columns'>
        @if(isset($profile_owner))
        <h4>Clips uploaded by: {{{ $profile_owner }}}</h4>
        @else
        <h4>All Clips</h4>
        @endif
        
    </div>
    </div>
@endsection

@section('content')
<div class='container'>
    <div class='row'>
        

        @if(!$posts->getCollection()->all())
            <div class="small-12 columns">
                <h4>No posts uploaded yet</h4>
            </div>
        @endif
    </div>
    
    <div class="small-12 columns">
            <?php echo $posts->render() ?>
        </div>
    @foreach(array_chunk($posts->getCollection()->all(), 3) as $row)
        <div class='row'>
            
            @foreach($row as $i)
            <article class='medium-4 columns'>
                
                <div class="clip_wrapper">
                    <?php
                    if(file_exists(base_path().'/public/files/clips/'.$i->name))
                    {
                        echo '<video class="video-clip" controls>';
                        echo    '<source src="/files/clips/'.$i->name. '" type="video/'.explode('.', $i->name)[1]. '" >'; 
                        echo    'Your browser does not support the video tag.';
                        echo '</video>';
                    }else{
                        echo    '<h5>File<br>'.$i->name.'<br>Deleted</h5>';
                        
                        $i->user_name = NULL;
                        $i->description = NULL;
                        $i->created_at = NULL;
                    }
                    ?>
                </div>
                <div>
                    <p id="sub_vid_text">
                        <a href="/clip/{{$i->name}}">{{{$i->name}}}</a><br>
                        Created on: {{{$i->created_at}}} by: <a href='/profile/{{{$i->user_name}}}'>{{{$i->user_name}}}</a> & viewed {{{$i->views}}} times.
                    </p>
                    <p id="video_description">{{{$i->description}}}</p>
                </div>
            </article>
            @endforeach
        </div>    
    @endforeach
    
    <?php echo $posts->render() ?>
    
</div>
@endsection


