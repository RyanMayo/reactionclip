<!DOCTYPE html>

<html>
    @include('partials.head')
    <body>
    <div class='single_post_container'>
        @yield('content')
    </div>
    @include('partials.bottomscript')
    </body>
</html>