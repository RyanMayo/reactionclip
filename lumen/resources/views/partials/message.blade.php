
@if (session('status'))
<div class='small-8 small-offset-2 columns'>
    <div class="alert-box" data-alert >
        <A HREF="javascript:history.go(0)" class="close" >&times;</A>
        {{ session('status') }}
        
    </div>
</div>
@endif


@if (count($errors) > 0)
<div class='small-8 small-offset-2 columns'>
    <div class="alert-box" data-alert >
        <!--<div class="alert alert-danger">-->
            <A HREF="javascript:history.go(0)" class="close" >&times;</A>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            
        <!--</div>-->
    </div>
</div>
@endif