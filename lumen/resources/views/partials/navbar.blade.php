<?php
    if(!isset($user_name)){
        $user_name = NULL;
    }
?>

<div class="top-bar"> 
    <div class="top-bar">
        <div class="top-bar-left">
          <ul class="menu" data-dropdown-menu>
              <li class="menu-text"><a href="/">Reactionclip.com</a></li>
          </ul>
        </div>
        <div class="top-bar-right">
            <ul class="menu">
                @if(isset($user_name))
                <li><a href='/profile/{{{$user_name}}}'>{{{$user_name}}}</a></li>
                @endif
                <li><a href="/clip/getvideo">Upload</a></li>
                <li><a href="/logout">Login/out</a></li>
            </ul>
        </div>
    </div>
</div>
