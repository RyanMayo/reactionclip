<!DOCTYPE html>

<?php
    use Illuminate\Http\Request;
?>
<html>
    <head>
        @include('partials.head')
    </head>
    
    <body>
        
        
        
        @include('partials.navbar')
        
        
        
        @include('partials.message')
        
        @yield('title')

        @yield('content')
            
        
        <footer>
            
        </footer>
        @include('partials.bottomscript')
        
    </body>
</html>
