<?php

/**
 * this is an edited version of the cURL version of Nitesh's 
 * http://blogs.niteshapte.com/2015-04-06-how-to-download-youtube-videos-using-wget-and-curl.htm
 *
 * @package YouTubeVideoDownloader
 * @author Nitesh Apte
 * @copyright 2015
 * @version 1
 * @license GPL v3
 */

namespace App\Models;

//C:\xampp\htdocs\reactionclip\lumen\app\Models\YouTubeVideoDownloaderModel.php
class YouTubeVideoDownloader  {
    
    private $videoID;
    private $supportedVideoFormat = array("5", "18", "34");
    private $downloadUrl;
    private $videoFormat;
    private $title;
    private $ext;
    private $alias;
    private $return_data = [];
    public  $error_data;
    
    public function __construct($videoID, $videoFormat = '34') {
        
        $this->videoID = $videoID;
        
        if(!in_array($videoFormat, $this->supportedVideoFormat)) {
            $videoFormat = "5";
        }
        
        $this->videoFormat = $videoFormat;
        
        $this->createDownloadLink();
    }
    
    
    private function getContents($urlForCurl){
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlForCurl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION,3);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0); 
        $infoPage = curl_exec ($ch);

        $curlError = curl_error($ch);
        curl_close ($ch);
        
        //$this->easyVar($curlError, '$curl error in getContents()');
        //$this->easyVar($infoPage, '$infoPage in getContents()');
        
        return $infoPage;
    }
    
    //added elseif($ext == 'webm')
    private function mp4Check($ext){
        if($ext == 'mp4'){
            return '.mp4';
        }elseif($ext == 'webm'){
            return '.webm';
        }else{
            return FALSE;
        }
    }
    
    public function createDownloadLink() {
        $urlForCurl = "http://www.youtube.com/get_video_info?video_id=".$this->videoID;
        
        $infoPage = $this->getContents($urlForCurl);
        
        //$this->easyVar($infoPage, '$infoPage');
        
        parse_str($infoPage, $arr);
        
        if(isset($arr['errorcode'])){
            $this->error_data = 'Problem with URL';
            return $this->error_data;
        }
        
        $this->title = $arr['title'];
        $this->alias = $this->getToken(10). '_'. date("Y-m-d_H-i-s");
        
        $urlData = $arr['url_encoded_fmt_stream_map'];
        $dataSet = explode(',', $urlData);
        parse_str(urldecode($dataSet[0]), $data);
        
        //$this->easyVar($data, '$data');
        
        $this->ext = $this->mp4Check(explode('/', $data['mime'])[1]);
        if(!$this->ext){
            //RYAN if not mp4 or webm this shall return a message of invalid.
            $this->error_data = 'reactionclip only takes mp4 & webm files at the moment';
            return $this->error_data;
        }
        
        $vidFormat = $data['itag'];
        $url = $data['url'];
        $sig = $data['signature'];
        
        $this->downloadUrl = str_replace('%2C', ',' ,$url.'&'.http_build_query($data).'&signature='.$sig.'&fmt='. $vidFormat );
        
        //$this->easyVar($this->downloadUrl, '$downloadUrl');
    }
   
    public function curlDownload() {
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->downloadUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION,3);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $data = curl_exec ($ch);
        $error = curl_error($ch);
        curl_close ($ch);
        
        $destination = realpath(__DIR__.'/../../../'). '/lumen/public/files/clip_original/'. $this->alias. $this->ext;
        
        
        $file = fopen($destination, "w+");
        fwrite($file, $data);
        fclose($file);
        
        //$this->easyVar($error, '$cUrl error in curlDownload');
        
        if(!$error){
            $this->return_data['error'] = FALSE;
            $this->return_data['destination'] = $destination;
            $this->return_data['title'] = $this->alias.$this->ext;
        }else{
            $this->return_data['error'] = $error;
        }
        
        return $this->return_data;
        
    }
    
    
    
    private function crypto_rand_secure($min, $max){
        
//lifted and edited from: http://solvedstack.com/questions/php-how-to-generate-a-random-unique-alphanumeric-string
        
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }
    
    private function getToken($length){

    //lifted and edited from: http://solvedstack.com/questions/php-how-to-generate-a-random-unique-alphanumeric-string

        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet) - 1;
        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max)];
        }
        return $token;
    }
    
}
