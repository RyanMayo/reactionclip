<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Clip extends Model {
    protected $table = 'clips';
    protected $fillable = [
        'name',
        'user_name',
        'description',
    ];
}