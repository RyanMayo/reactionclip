<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Clip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator;

class PostController extends Controller
{
   
    
    public function insertClip(Request $request)
    {
        $input['file'] = $_POST['file'];
        
        $input['desc'] = $_POST['desc'];
        
        $rules = [
            'file' => [
                'regex:(^[a-zA-Z0-9_.-]*$)'
                ],
        ];
        $validator = Validator::make($input, $rules);
        
        if( $validator->fails() ){
            return redirect('clip/getvideo')->withErrors($validator);
        }
        
        $input['user_name'] = $request->session()->get('user_name'); //RYAN
        
        if(!$input['user_name']){
            return redirect('login')->with('status', "User must log in to post");
        }
        
        if( is_file('files/clip_original/'.$input['file']) ){
            unlink('files/clip_original/'.$input['file']);
        }
        
        $clip = new Clip;
        $clip->name = $input['file'];
        $clip->description = $input['desc'];
        $clip->user_name = $input['user_name'];
        $clip->views = 1;
        $clip->save();
        
        return redirect('/profile/'.$input['user_name'])->with('status', 'Clip uploaded');
    }
    
//    public function getAllPosts()
//    {
//        //created_at is by time, views is by views
//        
//        $posts = Clip::orderBy('views', 'desc')->get();
//        return view('post.posts', ['posts'=>$posts]);
//    }
    
    public function getSomePosts(Request $request)
    {
        
        $posts = Clip::orderBy('views', 'desc')->paginate(9);
        
        //deletes posts without a clip associated
        foreach($posts as $i){
            if(!file_exists(base_path().'/public/files/clips/'.$i->name)){
                echo $i->name;
                echo '<br>';
                Clip::where('name', '=', $i->name)->delete();
            }
        }
        
        return View::make('post.someposts',[
            'posts' => $posts,
            'user_name' => $this->user_name
        ]);
    }
    
    public function getSingle($clip_id){
        $post = Clip::where('name', '=', $clip_id)->firstorfail();
        $post->views += 1;
        $post->save();
        return view('post.post', ['post'=>$post]);
    }
    
    public function myposts($profile_owner, Request $request)
    {
        
        $posts = DB::table('clips')->where('user_name', $profile_owner)->paginate(9);
        
        return View::make('post.someposts',[
            'posts' => $posts,
            'user_name' => $this->user_name,
            'profile_owner' => $profile_owner
        ]);
    }
}
