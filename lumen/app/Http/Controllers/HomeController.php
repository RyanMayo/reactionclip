<?php

namespace App\Http\Controllers;
use Cache;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function showWelcome()
    {
        echo 'derp';
        //this returns the view hello.php in resources/views and sends the variable $name through
        return view('hello', ['name' => 'ryan']);
    }
    
    public function testCache()
    {
        //Cache::store('database')->put('bar', 'whatever the fuq', 10);
        $derp = Cache::store('database')->get('bar');
        $squirp = Cache::get('bar');
        var_dump($derp);
        echo '<br>';
        var_dump($squirp);
    }
    
    public function testSession(Request $request)
    {
        $request->session()->put('foo', 'barista');
        $foo = $request->session()->get('foo');
        var_dump($foo);
    }
    
    public function testUser(Request $request){
        $me = $request->session()->get('user_name');
        var_dump($me);
    }
}
