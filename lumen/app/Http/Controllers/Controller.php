<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
     public function __construct(Request $request) {
        if($request->session()->has('user_name')){
            $this->user_name = $request->session()->get('user_name');
        }else{
            $this->user_name = NULL;
        }
        
        if($request->session()->has('user_id')){
            $this->user_id = $request->session()->get('user_id');
        }else{
            $this->user_id = NULL;
        }
    }
}
