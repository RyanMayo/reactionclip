<?php

namespace App\Http\Controllers;

use App\Models\YouTubeVideoDownloader;
use Validator;


class ClipController extends Controller
{
    public function getVideo()
    {
        return view('clip.getvideo');
    }
    
    
    public function getOriginal()
    {
        
        $input['video_file'] = $_POST['file'];
        
        $rules = [
            'video_file' => [
                'required',
                'alpha_dash'
                ],
        ];
        $validator = Validator::make($input, $rules);
        
        if( $validator->fails() ){
            return redirect('clip/getvideo')->withErrors($validator);
        }
        
        include(base_path(). '/app/Models/YouTubeVideoDownloaderModel.php');
        $obj = new YouTubeVideoDownloader($input['video_file']);
        
        if($obj->error_data){
            return redirect('clip/getvideo')->with('status', $obj->error_data);
        }
        
        $vid_data = $obj->curlDownload();
        $input['clip_id'] = $vid_data['title'];
        return view('clip.edit', ['input'=> $input] );
    }
    
    public function makeClip()
    {
        
        $input['clip_id'] = $_POST['clip_id'];
        $input['start_cut'] = $_POST['start_cut'];
        $input['end_cut'] = $_POST['end_cut'];
        $input['length'] = $input['end_cut'] - $input['start_cut'];
       
        $rules = [
            'clip_id' => [
                'required',
                'regex:(^[a-zA-Z0-9_.-]*$)',
                ],
            'start_cut' => [
                'required',
                'numeric',
                'min:0'
                ],
            'end_cut' => [
                'required',
                'numeric',
                'min:0',
            ],
            'length'=> [
                'max:35',
                'min:0',
            ]
        ];
        $validator = Validator::make($input, $rules);
        
        if( $validator->fails() ){
            return view('clip.edit', ['input'=>$input])->withErrors($validator);
        }
        
        $input['end_cut'] -= $input['start_cut'];

        $script = 'ffmpeg -i files/clip_original/'. $input['clip_id'].
                ' -ss '. $input['start_cut']. ' -t '. $input['end_cut']. 
                ' -c:v copy -c:a copy files/clips/'. $input['clip_id'];

        if( is_file('files/clips/'.$input['clip_id']) ){
            unlink('files/clips/'.$input['clip_id']);
        }

        exec($script);
        return view('clip.checkandlog', ['input'=>$input]);
        
    }
    
    public function editClip($clip_id)
    {
        $input['clip_id'] = $clip_id;
        return view('clip.edit', ['input'=>$input]);
    }
    
}