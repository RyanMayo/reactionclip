<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use DB;
use Validator;

class UserController extends Controller
{
    
    public function __construct(Request $request) {
        parent::__construct($request);
        
    }
    
    public function login(Request $request){
        $input['user_name'] = $_POST['user_name'];
        $input['user_password'] = $_POST['user_password'];
        
        if($input['user_name'] && $input['user_password']){
            
            $data_ob = DB::table('users')->where('name', $input['user_name'] )->first();
            if($data_ob)
            {
                $this->data['password'] = $data_ob->password;
            }else{
                $this->data['password'] = FALSE;
            }
            
            if( password_verify($input['user_password'], $this->data['password']) )
            {
            //if($input['user_password'] == $this->data['password']){
                
                $request->session()->put('user_name', $data_ob->name);
                $request->session()->put('user_id', $data_ob->id);
                $request->session()->flash('status', 'Login successful!');

                //var_dump( $request->session()->get('user') );
            }else{
                return redirect('login')->with('status', "Password doesn't match");
            }
            return redirect('/')->with('status', "User logged in");
        }else{
            return redirect('login')->with('status', 'User name & password must be input');
        }
    }
    
    public function logout(Request $request)
    {
        
        $request->session()->flush();
        //Ryan: on cookie implementation put cookie delete here..
        
        return redirect('login')->with('status', "User logged out");
    }
    
    public function register(Request $request)
    {
        $input['user_name'] = $_POST['user_name'];
        $input['user_password'] = $_POST['user_password'];
        $input['confirm_password'] = $_POST['confirm_password'];
        
        $rules = [
            'user_name' => [
                'required',
                'unique:users,name',
                'alpha_dash',
                'between:8,40'
                ],
            'user_password' => [
                'required',
                'alpha_dash',
                'between:8,200'
                ],
            'confirm_password' => [
                'same:user_password'
            ]
        ];
        
        $validator = Validator::make($input, $rules);
        
        if( $validator->fails() ){
            return redirect('register')->withErrors($validator);
        }
        
        $user_ob = new User;
        $user_ob->name = $input['user_name'];
        $user_ob->password = password_hash($input['user_password'], PASSWORD_DEFAULT);
        $user_ob->save();
        
        $request->session()->put('user_name', $user_ob->name);
        $request->session()->put('user_id', $user_ob->id);
        return redirect('/')->with('status', "User created and logged in");
    }
}
