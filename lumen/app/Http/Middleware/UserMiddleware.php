<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if($request->session()->get('user_name')){
            return $next($request);
        }else{
            return redirect('/login')->with('status','User must log in to upload clips');
        }
        
        //return $next($request);
    }
}
