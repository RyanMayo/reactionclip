<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


/*************** Entry Route ************************/

$app->get('/', 'PostController@getsomeposts');

/*************** Login/out Routes ************************/

$app->get('/login', function(Request $request){
    
    if($request->session()->has('user_name')){
        return redirect('/')->with('status','Already logged in');
    }
    return view('user.login');
});

$app->post('/login', 'UserController@login');

$app->get('/logout', 'UserController@logout');

/*************** User Routes ************************/

$app->get('/profile/{profile_owner}', 'PostController@myposts');

$app->get('/register', function(){
    return view('user.register');
});

$app->post('/register', 'UserController@register');

/*************** Post Routes ************************/


$app->post('/posts/insertpost', ['middleware'=>'userMid','uses'=>'PostController@insertPost']);

/*************** Clip (video) Routes ************************/

$app->get('/clip/getvideo', ['middleware'=>'userMid','uses'=>'ClipController@getVideo']);

$app->post('/clip/getoriginal', ['middleware'=>'userMid','uses'=>'ClipController@getOriginal']);

$app->post('/clip/edit/{clip_id}', ['middleware'=>'userMid','uses'=>'ClipController@editClip']);

$app->post('/clip/make', ['middleware'=>'userMid','uses'=>'ClipController@makeClip']);

$app->post('posts/insertclip', ['middleware'=>'userMid','uses'=>'PostController@insertClip']);

$app->get('/clip/{clip_id}', 'PostController@getSingle');


/*************** Catchall for unnamed routes ************************/

$app->get('/{anything_else}', 'PostController@getsomeposts');