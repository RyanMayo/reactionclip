<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReactionclipsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('user_name');
            $table->text('description');
            $table->integer('views');
            $table->timestamps();
        });
        
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('password');
            $table->timestamps();
        });
        
        Schema::create('remember_me', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('token');
            $table->timestamps();
        });
        
        Schema::create('cache', function($table) {
            $table->string('key')->unique();
            $table->text('value');
            $table->integer('expiration');
        });
        
        Schema::create('sessions', function ($table) {
            $table->string('id')->unique();
            $table->text('payload');
            $table->integer('last_activity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clips');
        Schema::drop('users');
        Schema::drop('remember_me');
        Schema::drop('cache');
        Schema::drop('sessions');
    }
}
